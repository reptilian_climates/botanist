

'''
	https://docs.python.org/3/library/unittest.html#module-unittest
'''

'''
	python -m unittest UT_status.py

	python -m unittest *status.py
'''


STATUS = "LOCAL"
#STATUS = "PYPI"

MODULES_PATHS = []
if (STATUS == "LOCAL"):
	MODULES_PATHS = [ 'THIS_MODULE' ]
if (STATUS == "PYPI"):
	MODULES_PATHS = [ 'THIS_MODULE_PIP' ]

def add_paths_to_system (paths):
	import pathlib
	this_folder = pathlib.Path (__file__).parent.resolve ()

	from os.path import dirname, join, normpath
	import sys
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))


add_paths_to_system ([ 
	'../fields/gardens',
	'../fields/gardens_pip'	
])

import sys


import botanist.processes.multiple as multi_proc
from botanist.ports.find_an_open_port import find_an_open_port
		
	

import time
import unittest
class CONSISTENCY (unittest.TestCase):
	def test_1 (this):
		procs = multi_proc.start (
			processes = [
				{ 
					"string": f'python3 -m http.server { find_an_open_port () }',
					"CWD": None
				},
				{
					"string": f'python3 -m http.server { find_an_open_port () }',
					"CWD": None
				}
			]
		)
		
		exit = procs ["exit"]
		processes = procs ["processes"]

		time.sleep (.5)
		
		exit ()